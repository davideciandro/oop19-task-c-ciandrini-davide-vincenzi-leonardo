﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ciandrini_Davide.flappyBird-Test
{
    [TestClass()]
    public class BirdImplTests
    {
        [TestMethod()]
        public void BirdUpdateTest()
        {
            int n = 10;
            int x = 60;
            int expected;

            BirdImpl bird = new BirdImpl();
            bird.PosY = x;
            expected = bird.PosY + n;
            bird.PosY = expected;
            Assert.IsTrue(expected == bird.PosY , "BirdUpdate() non funziona");
        }
    }
}