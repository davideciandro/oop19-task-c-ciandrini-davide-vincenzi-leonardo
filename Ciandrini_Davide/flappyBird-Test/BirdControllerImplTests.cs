﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace Ciandrini_Davide.flappyBird-Test
{
    [TestClass()]
    public class BirdControllerImplTests
    {
        public static int SCREEN_HEIGHT = 335;

        [TestMethod()]
        public void BirdMovementTest()
        {
            int n = 5;

            BirdImpl bird = new BirdImpl();
            BirdViewImpl birdView = new BirdViewImpl();
            Rectangle r = new Rectangle();

            bird.BirdUpdate(n);
            birdView.UpdatePosition(bird.PosY);
            Assert.IsTrue(bird.PosY == birdView.R.Y, "BirdMovement() non funziona");
        }

        [TestMethod()]
        public void FloorCollisionTest()
        {
            int num;
            Rectangle rectangle = new Rectangle();
            BirdImpl bird = new BirdImpl();

            num = SCREEN_HEIGHT - bird.Height;
            rectangle.Y = num;
            bird.PosY = num;

            Assert.IsTrue(rectangle.Y == bird.PosY, "FloorCollision() non funziona");
        }
    }
}