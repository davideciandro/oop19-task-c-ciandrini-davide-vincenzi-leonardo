﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace Ciandrini_Davide.flappyBird-Test
{
    [TestClass()]
    public class BirdViewImplTests
    {
        [TestMethod()]
        public void SetHeightWidthTest()
        {
            int height = 3;
            int width = 4;

            BirdViewImpl birdView = new BirdViewImpl();
            Rectangle r = new Rectangle();

            birdView.SetHeightWidth(height, width);
            r.Height = height;
            r.Width = width;

            Assert.IsTrue(r.Height == birdView.R.Height, "Non setta correttamente l'altezza");
            Assert.IsTrue(r.Width == birdView.R.Width, "Non setta correttamente la larghezza");

        }
    }
}