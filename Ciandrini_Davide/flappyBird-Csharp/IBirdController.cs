﻿using System.Drawing;

namespace Ciandrini_Davide.flappyBird-Csharp
{
    interface IBirdController
    {
        void InitBirdView();
        void BirdMovement(int n);
        bool FloorCollision(Rectangle r);
    }
}
