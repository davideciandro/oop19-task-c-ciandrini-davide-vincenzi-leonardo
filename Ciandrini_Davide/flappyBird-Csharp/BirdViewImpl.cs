﻿using System.Drawing;


namespace Ciandrini_Davide.flappyBird-Csharp
{
    public class BirdViewImpl : IBirdView
    {
        private Rectangle r;
        
        public BirdViewImpl()
        {
            r = new Rectangle();
        }

        public void SetPosition(int x, int y)
        {
            r.X = x;
            r.Y = y;
        }

        public void SetHeightWidth(int height, int width)
        {
            r.Height = height;
            r.Width = width;
        }

        public void SetImage(string images)
        {
            Image imageFile = Image.FromFile(images);
            TextureBrush textureBrush = new TextureBrush(imageFile);
            Graphics graphics = Graphics.FromImage(imageFile);
            graphics.FillRectangle(textureBrush, r);
        }

        public void UpdatePosition(int y)
        {
            r.Y = y;
        }

        public Rectangle R
        {
            get => r;
        }
    }
}
