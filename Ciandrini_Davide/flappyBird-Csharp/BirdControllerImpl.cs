﻿using System.Drawing;

namespace Ciandrini_Davide.flappyBird-Csharp
{
    public class BirdControllerImpl : IBirdController
    {
        private static readonly int SCREEN_HEIGHT = 335;
        private BirdImpl bird;
        private BirdViewImpl birdView;

        public BirdControllerImpl()
        {
            bird = new BirdImpl();
            birdView = new BirdViewImpl();
            InitBirdView();
        }

        public void InitBirdView()
        {
            birdView.SetPosition(bird.PosX, bird.PosY);
            birdView.SetHeightWidth(bird.Height, bird.Width);
            birdView.SetImage(bird.BirdImagePath);
        }

        public void BirdMovement(int n)
        {
            bird.BirdUpdate(n);
            birdView.UpdatePosition(bird.PosY);
        }

        public bool FloorCollision(Rectangle r)
        {
            return r.Y == SCREEN_HEIGHT - bird.Height;
        }

        public BirdViewImpl BirdView
        {
            get => birdView;
        }

        public BirdImpl BirdImpl
        {
            get => bird;
        }
    }

}
