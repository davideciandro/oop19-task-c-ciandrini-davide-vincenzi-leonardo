﻿namespace Ciandrini_Davide.flappyBird-Csharp
{
    interface IBird
    {
        void BirdUpdate(int n);

        int PosY { get; }
        int PosX { get; }
        int Width { get; }
        int Height { get; }
		string BirdImagePath { get; }
    }
}
