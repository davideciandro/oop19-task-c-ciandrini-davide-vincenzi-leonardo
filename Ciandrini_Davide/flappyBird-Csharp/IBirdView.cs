﻿namespace Ciandrini_Davide.flappyBird-Csharp
{
    interface IBirdView
    {
        void SetPosition(int x, int y);
        void SetHeightWidth(int height, int width);
        void UpdatePosition(int y);
    }
}
