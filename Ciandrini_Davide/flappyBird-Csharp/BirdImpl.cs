﻿using System;

namespace Ciandrini_Davide.flappyBird-Csharp
{
    public class BirdImpl : IBird
    {
        private static readonly int INITIAL_POSITION = 50;
        private static readonly int WIDTH_BIRD = 45;
        private static readonly int HEIGHT_BIRD = 32;

        public int PosY { get; private set; }
        public int PosX { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public string BirdImagePath { get;}

        public BirdImpl()
        {
            PosY = INITIAL_POSITION;
            PosX = INITIAL_POSITION;
            Width = WIDTH_BIRD;
            Height = HEIGHT_BIRD;
            BirdImagePath = "bird.png";
        }

        public void BirdUpdate(int n)
        {
           PosY = PosY + n;
        }
    }
}
