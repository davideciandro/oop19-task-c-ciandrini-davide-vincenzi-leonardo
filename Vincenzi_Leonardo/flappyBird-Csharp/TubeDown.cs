﻿namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    public class TubeDown : AbstractTube
    {
        private static readonly int DISTANCE = 105;

        public TubeDown(string _tubeImagePath) : base(_tubeImagePath)
        { }
        public override AbstractTube Copy()
        {
            return new TubeDown(this.TubeImagePath);
        }

        public void Y(int y)
        {
           PosY = (y + DISTANCE);
        }
    }
}
