﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    public class TubeView : ITubeView
    {
        private Rectangle r;
        private Rectangle r2;
        public TubeView()
        {
            r = new Rectangle();
            r2 = new Rectangle();
        }
    public void CreateRectangles()
        {
            r = new Rectangle();
            r2 = new Rectangle();
        }

        public void SetTubeDownDimension(int width, int height)
        {
            r2.Width = width;
            r2.Height = height;    
        }

        public void SetTubeDownImage(string image)
        {
            Image imagefile = Image.FromFile(image);
            TextureBrush tBrush = new TextureBrush(imagefile);
            Graphics graphics = Graphics.FromImage(imagefile);
            graphics.FillRectangle(tBrush, r2);
        }

        public void SetTubeDownPosition(int x, int y)
        {
            r2.X = x;
            r2.Y = y;
        }

        public void SetTubeUpDimension(int width, int height)
        {
            r.Width = width;
            r.Height = height;
        }

        public void SetTubeUpImage(string image)
        {
            Image imagefile = Image.FromFile(image);
            TextureBrush tBrush = new TextureBrush(imagefile);
            Graphics graphics = Graphics.FromImage(imagefile);
            graphics.FillRectangle(tBrush, r);
        }

        public void SetTubeUpPosition(int x, int y)
        {
            r.X = x;
            r.Y = y;
        }

        public Pair<Rectangle, Rectangle> GetRectangles()
        {
            return new Pair<Rectangle, Rectangle>(r, r2);
        }
    }
}
