﻿using System;
using System.Drawing;

namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    class TubeController : ITubeController
    {
        private readonly TubeUp tubeUp;
        private readonly TubeDown tubeDown;
        //private readonly TubeMap tubeMap;
        private readonly TubeView tubeView;


        public TubeController(/*final FlappyBirdController controller*/)
        {
            tubeUp = new TubeUp("top.png");
            tubeDown = new TubeDown("bottom.png");
            //tubeMap = new TubeMapImpl(controller);
            tubeView = new TubeView();
           //tubeMap.addToMap(createTubePair());
        }


        private Pair<Rectangle,Rectangle> CreateTubePair()
        {
            TubeUp tubeUpCopy = (TubeUp)tubeUp.Copy();
            TubeDown tubeDownCopy = (TubeDown)tubeDown.Copy();
            tubeUpCopy.Y();
            tubeDownCopy.Y(tubeUpCopy.PosY);

            tubeView.CreateRectangles();

            tubeView.SetTubeUpDimension(tubeUpCopy.Width, tubeUpCopy.Height);
            tubeView.SetTubeDownDimension(tubeDownCopy.Width, tubeDownCopy.Height);

            tubeView.SetTubeUpPosition(tubeUpCopy.PosX, tubeUpCopy.PosY - tubeUpCopy.Height);
            tubeView.SetTubeDownPosition(tubeDownCopy.PosX, tubeDownCopy.PosY);

            tubeView.SetTubeUpImage(tubeUp.TubeImagePath);
            tubeView.SetTubeDownImage(tubeDown.TubeImagePath);

            return tubeView.GetRectangles();
        }

        public void AddTube()
        {
            throw new NotImplementedException();
        }

        public void ScrollTubes()
        {
            throw new NotImplementedException();
        }
    }
}
