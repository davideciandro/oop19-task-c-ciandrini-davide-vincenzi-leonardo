﻿using System;
namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    public class TubeUp : AbstractTube
    {
        private static readonly int MAX = 175;
        private static readonly int MIN = 60;
        Random rnd = new Random();

        public TubeUp(string _tubeImagePath) : base(_tubeImagePath)
        { }
        public override AbstractTube Copy()
        {
            return new TubeUp(this.TubeImagePath);
        }

        public void Y()
        {
           PosY = (rnd.Next(MIN,MAX));
        }
    }
}
