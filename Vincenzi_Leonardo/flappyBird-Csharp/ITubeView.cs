﻿using System.Drawing;

namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    interface ITubeView
    {
        void CreateRectangles();

        void SetTubeUpPosition(int x, int y);

        void SetTubeUpDimension(int width, int height);

        void SetTubeDownPosition(int x, int y);

        void SetTubeDownDimension(int width, int height);

        void SetTubeUpImage(string image);

        void SetTubeDownImage(string image);

        Pair<Rectangle, Rectangle> GetRectangles();

    }
}
