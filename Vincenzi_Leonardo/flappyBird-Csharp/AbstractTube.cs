﻿namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    public abstract class AbstractTube
    {
        private static readonly int HEIGHT = 175;
        private static readonly int WIDTH = 55;
        private static readonly int POS_X = 600;
        private int _posY;


        public AbstractTube(string _tubeImagePath)
        {
            TubeImagePath = _tubeImagePath;
        }

        public abstract AbstractTube Copy();

        public int PosX
        {
            get => POS_X;
        }

        public int PosY
        {
            get => _posY;
            set => _posY = value;
        }

        public int Width
        {
            get => WIDTH;
        }

        public int Height
        {
            get => HEIGHT;
        }

        public string TubeImagePath { get; }


    }
}
