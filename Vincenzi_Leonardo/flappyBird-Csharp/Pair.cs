﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vincenzi_Leonardo.flappyBird-Csharp
{
    public class Pair<X, Y>
    {
        private readonly X x;
        private readonly Y y;

        public Pair(X x, Y y) : base()
        {
            this.x = x;
            this.y = y;
        }

        public X GetX
        {
            get { return x; }
        }

        public Y GetY
        {
            get { return y; }
        }

        public int HashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + ((x == null) ? 0 : x.GetHashCode());
            result = prime * result + ((y == null) ? 0 : y.GetHashCode());
            return result;
        }

        public override string ToString()
        {
            return "Pair [x=" + x + ", y=" + y + "]";
        }




    }
}
