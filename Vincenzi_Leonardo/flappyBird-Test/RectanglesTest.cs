﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using WindowsFormsApp1.Vincenzi_Leonardo;

namespace Vincenzi_Leonardo.flappyBird-Test
{
    [TestClass()]
    public class RectanglesTest
    {
        TubeUp tubeUp = new TubeUp("imagepath");
        TubeDown tubeDown = new TubeDown("imagepath");
        Rectangle r = new Rectangle();
        Rectangle r2 = new Rectangle();
        TubeView tubeView = new TubeView();

        [TestMethod()]
        public void testRectangle()
        {
            tubeDown.Y(tubeUp.PosY);
            tubeView.CreateRectangles();
            tubeView.SetTubeDownDimension(tubeUp.Width, tubeUp.Height);
            tubeView.SetTubeUpDimension(tubeUp.Width, tubeUp.Height);
            tubeView.SetTubeDownPosition(tubeDown.PosX, tubeDown.PosY);
            tubeView.SetTubeUpPosition(tubeUp.PosX, tubeUp.PosY);
            r = tubeView.GetRectangles().GetX;
            r2 = tubeView.GetRectangles().GetY;
            Assert.AreEqual(r.Width, tubeUp.Width);
            Assert.AreEqual(r2.Width, tubeDown.Width);
            Assert.AreEqual(r.Height, tubeUp.Height);
            Assert.AreEqual(r2.Height, tubeDown.Height);
            Assert.AreEqual(r.X, tubeUp.PosX);
            Assert.AreEqual(r.Y, tubeUp.PosY);
            Assert.AreEqual(r2.X, tubeDown.PosX);
            Assert.AreEqual(r2.Y, tubeDown.PosY);
        }
    }
}
