﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApp1.Vincenzi_Leonardo;

namespace Vincenzi_Leonardo.flappyBird-Test
{
    [TestClass()]
    public class TubesPositionsTest
    {
        TubeUp tubeUp = new TubeUp("imagepath");
        TubeDown tubeDown = new TubeDown("imagepath");
        private readonly static int DISTANCE = 105;


        [TestMethod()]
        public void Positions()
        {
            tubeUp.Y();
            int initUpPosY = tubeUp.PosY;
            int initDownPosY = tubeDown.PosY;
            Assert.IsNotNull(initUpPosY, "La Y del tubo sopra non è null");
            Assert.IsNotNull(initDownPosY, "La Y del tubo sotto non è null");
            int downPosY = tubeUp.PosY + DISTANCE;
            tubeDown.Y(tubeUp.PosY);
            initDownPosY = tubeDown.PosY;
            Assert.AreEqual(downPosY, initDownPosY);
            tubeUp.PosY = tubeUp.PosY - 10;
            tubeDown.Y(tubeUp.PosY);
            Assert.AreNotEqual(downPosY, tubeDown.PosY);
        }
    }
}